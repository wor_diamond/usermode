const fs = require("fs-extra");
const path = require("path");
const glob = require("glob");
const crypto = require("crypto");

function hash(data) {
	const hash = crypto.createHash("sha256");
	hash.update(data);
	return hash.digest("hex");
}

// pushd
let oldPath = process.cwd();
process.chdir(path.resolve(__dirname, ".."));


try {
	fs.removeSync("./dist-old");
	fs.renameSync("./dist", "./dist-old");
} catch(e) {
}

console.log("Copying");
fs.copySync("./src", "./dist");

console.log("Searching");
let files = glob.sync("./dist/**/*");
files.forEach(file => {
	let contents = fs.readFileSync(file, {
		encoding: "utf-8"
	});

	// If the first line is //@babel
	if(contents.trim().split("\n")[0].toLowerCase().replace(/\s/g, "") == "//@babel") {
		let hashed = hash(contents);
		let prefix = "var _DIGEST_=\"" + hashed + "\";(function(){";
		let suffix = "})()";

		if(process.argv[2] == "incremental") {
			try {
				let originalFile = file.replace(/^.\/dist\//, "./dist-old/");
				let original = fs.readFileSync(originalFile, {
					encoding: "utf-8"
				});

				if(original.indexOf(prefix) == 0) {
					// Cache hit
					console.log("Use compiled " + file);
					fs.renameSync(originalFile, file);
					return;
				}
			} catch(e) {
			}
		}

		console.log("Transforming " + file);

		contents = require("babel-core").transform(contents, {
			plugins: [
				["transform-builtin-extend", {
					globals: ["Error", "Array"],
					approximate: true
				}],
				"transform-class-properties",
				"syntax-async-functions",
				"syntax-async-generators",
				"transform-async-to-generator",
				"transform-es2015-modules-commonjs",
				"transform-es2015-classes",
				"transform-es2015-block-scoping",
				"transform-es2015-for-of",
				"transform-es2015-shorthand-properties",
				"transform-es2015-arrow-functions"
			]
		}).code;


		console.log("Minifying");
		let result = require("uglify-es").minify(contents, {
			warnings: true
		});
		if(result.warnings) {
			console.warn(result.warnings.join("\n"));
		} else if(result.error) {
			throw result.error;
		}
		contents = result.code;


		contents = prefix + contents + suffix;

		console.log("Saving " + file);
		fs.writeFileSync(file, contents);
	}
});

console.log("Cleaning up");
fs.removeSync("./dist-old");

console.log("Done");

process.chdir(oldPath);
//@babel
const ShellBuiltins = {
	async __block__(children, parser) {
		let result = 0;
		for await(let child of children) {
			result = await parser.evaluateCommand(child);
		}
		return result;
	},


	async if(args, parser) {
		let ifTrue = null, ifFalse = null;
		if(typeof args[args.length - 1] != "function") {
			stdio.write(stdio.STDERR, "/bin/bash: if: expected function\n");
			return 1;
		} else {
			ifTrue = args[args.length - 1];
		}

		if(args[args.length - 2] == "else") {
			if(typeof args[args.length - 3] != "function") {
				stdio.write(stdio.STDERR, "/bin/bash: if: expected function\n");
				return 1;
			} else {
				ifFalse = ifTrue;
				ifTrue = args[args.length - 3];
			}
		}

		let command = args.slice(0, -1);
		if(ifFalse) {
			command = command.slice(0, -2);
		}
		if(command.length == 0) {
			stdio.write(stdio.STDERR, "/bin/bash: if: no condition\n");
			return 1;
		}

		if(await parser.evaluateCommand(parser.parse(command)) == 0) {
			return await ifTrue();
		} else if(ifFalse) {
			return await ifFalse();
		} else {
			return 0;
		}
	},

	async exit(args) {
		throw new ExitException(isNaN(Number(args[0])) ? 0 : Number(args[0]));
	},

	async true(args, parser) {
		return 0;
	},
	async false(args, parser) {
		return 1;
	},

	async echo(args) {
		stdio.write(stdio.STDOUT, args.join(" "));
	}
};

class ExitException extends Error {
	constructor(code) {
		super("exit " + code);
		this.exitCode = code;
	}
};


class Parser {
	constructor() {
		this.args = [];
		this.balance = 0;
		this.state = "next";
	}

	// Split into tokens
	getArgs(line) {
		line.split("").forEach(chr => {
			if(this.state == "goon") {
				// Simple text

				if(chr.trim() == "") {
					this.state = "next";
				} else {
					this.args[this.args.length - 1] += chr;
				}
			} else if(this.state == "next") {
				// After argument

				if(chr == "\"") {
					this.args.push("");
					this.state = "string";
				} else if(chr == "{") {
					this.args.push({action: "blockbegin"});
					this.balance++;
				} else if(chr == "}") {
					if(this.balance == 0) {
						throw new SyntaxError("Unmatched }");
					}
					this.args.push({action: "blockend"});
					this.balance--;
				} else if(chr.trim() != "") {
					this.args.push(chr);
					this.state = "goon";
				}
			} else if(this.state == "string") {
				// Inside string

				if(chr == "\"") {
					this.state = "strspace";
				} else {
					this.args[this.args.length - 1] += chr;
				}
			} else if(this.state == "strspace") {
				// Expect space after string

				if(chr.trim() == "") {
					this.state = "next";
				} else {
					throw new SyntaxError("Expected space after string literal, got: '" + chr + "'");
				}
			}
		});

		this.args.push({action: "next"});
	}

	feed(line) {
		this.getArgs(line);
		if(this.balance > 0 || this.state == "string") {
			return false;
		}

		return true;
	}


	parse(args) {
		let command = {
			name: "__block__",
			children: [],
			root: true
		};
		command.parent = command;

		let state = "name";
		args.forEach(arg => {
			if(state == "name") {
				// Command name

				if(arg.action == "blockbegin") {
					throw new SyntaxError("Unexpected block");
				} else if(arg.action == "blockend") {
					command = command.parent;
					state = "args";
				} else if(arg.action == "next") {
				} else {
					let inner = {
						name: arg,
						args: [],
						parent: command
					};
					command.children.push(inner);
					command = inner;

					state = "args";
				}
			} else if(state == "args") {
				// Command arguments

				if(arg.action == "blockbegin") {
					let inner = {
						name: "__block__",
						children: [],
						parent: command
					};
					command.args.push(inner);
					command = inner;

					state = "name";
				} else if(arg.action == "blockend") {
					command = command.parent;
				} else if(arg.action == "next") {
					command = command.parent;
					state = "name";
				} else {
					command.args.push(arg);
				}
			}
		});

		command = command.parent;

		if(!command.root) {
			throw new SyntaxError("Unmatched block");
		}

		return command;
	}

	async evaluate() {
		return await this.evaluateCommand(this.parse(this.args));
	}

	async evaluateCommand(command) {
		if(command.name == "__block__") {
			return await ShellBuiltins.__block__(command.children, this);
		} else if(command.name in ShellBuiltins) {
			command.args = command.args.map(arg => {
				if(typeof arg == "object") {
					return async () => {
						return await this.evaluateCommand(arg);
					};
				}

				return arg;
			});

			return await ShellBuiltins[command.name](command.args, this);
		} else {
			if(command.args.some(arg => typeof arg == "object")) {
				throw new Error(command.name + " is not a shell builtin and cannot receive functions as arguments");
			}

			if(command.name.startsWith("./") || command.name.startsWith("../") || command.name.startsWith("/")) {
				return await process.spawn(command.name, command.args, {
					[stdio.STDIN]: stdio.STDIN,
					[stdio.STDOUT]: stdio.STDOUT,
					[stdio.STDERR]: stdio.STDERR
				});
			}

			throw new Error(command.name + " is not a file or a shell builtin");
		}
	}
};


async function serve() {
	stdio.write(stdio.STDOUT, "\n$ ");

	const parser = new Parser;

	let code = "";
	let result;
	while(true) {
		let line = await stdio.read(stdio.STDIN);
		code += line;

		try {
			result = parser.feed(line);
			if(!result) {
				// Need more
				stdio.write(stdio.STDOUT, "... ");
				continue;
			} else {
				break;
			}
		} catch(e) {
			stdio.write(stdio.STDERR, "/bin/bash: error: " + e.message + "\n");
			return;
		}
	}

	try {
		let result = await parser.evaluate();
	} catch(e) {
		if(e instanceof ExitException) {
			throw e;
		} else {
			stdio.write(stdio.STDERR, "/bin/bash: error: " + e.message + "\n");
		}
	}
}

(async function() {
	await stdio.open(stdio.STDIN, "r");
	await stdio.open(stdio.STDOUT, "w");
	await stdio.open(stdio.STDERR, "w");

	stdio.write(stdio.STDOUT, "Wor Diamond  /bin/bash v0.1\n");

	while(true) {
		try {
			await serve();
		} catch(e) {
			if(e instanceof ExitException) {
				process.exit(e.exitCode);
				return;
			} else {
				throw e;
			}
		}
	}
})();